﻿namespace LotteryNumberGenerator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.titleLbl = new System.Windows.Forms.Label();
            this.num1Lbl = new System.Windows.Forms.Label();
            this.num2Lbl = new System.Windows.Forms.Label();
            this.num3Lbl = new System.Windows.Forms.Label();
            this.num4Lbl = new System.Windows.Forms.Label();
            this.num6Lbl = new System.Windows.Forms.Label();
            this.num5Lbl = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.resultLbl = new System.Windows.Forms.Label();
            this.generateBtn = new System.Windows.Forms.Button();
            this.saveBtn = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.statusLbl = new System.Windows.Forms.StatusStrip();
            this.SuspendLayout();
            // 
            // titleLbl
            // 
            this.titleLbl.AutoSize = true;
            this.titleLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.titleLbl.Location = new System.Drawing.Point(229, 25);
            this.titleLbl.Name = "titleLbl";
            this.titleLbl.Size = new System.Drawing.Size(228, 20);
            this.titleLbl.TabIndex = 0;
            this.titleLbl.Text = "Lottery Numbers Generator";
            // 
            // num1Lbl
            // 
            this.num1Lbl.AutoSize = true;
            this.num1Lbl.Location = new System.Drawing.Point(148, 70);
            this.num1Lbl.Name = "num1Lbl";
            this.num1Lbl.Size = new System.Drawing.Size(13, 13);
            this.num1Lbl.TabIndex = 1;
            this.num1Lbl.Text = "1";
            // 
            // num2Lbl
            // 
            this.num2Lbl.AutoSize = true;
            this.num2Lbl.Location = new System.Drawing.Point(205, 70);
            this.num2Lbl.Name = "num2Lbl";
            this.num2Lbl.Size = new System.Drawing.Size(13, 13);
            this.num2Lbl.TabIndex = 2;
            this.num2Lbl.Text = "2";
            this.num2Lbl.Click += new System.EventHandler(this.label3_Click);
            // 
            // num3Lbl
            // 
            this.num3Lbl.AutoSize = true;
            this.num3Lbl.Location = new System.Drawing.Point(260, 70);
            this.num3Lbl.Name = "num3Lbl";
            this.num3Lbl.Size = new System.Drawing.Size(13, 13);
            this.num3Lbl.TabIndex = 3;
            this.num3Lbl.Text = "3";
            // 
            // num4Lbl
            // 
            this.num4Lbl.AutoSize = true;
            this.num4Lbl.Location = new System.Drawing.Point(316, 70);
            this.num4Lbl.Name = "num4Lbl";
            this.num4Lbl.Size = new System.Drawing.Size(13, 13);
            this.num4Lbl.TabIndex = 4;
            this.num4Lbl.Text = "4";
            // 
            // num6Lbl
            // 
            this.num6Lbl.AutoSize = true;
            this.num6Lbl.Location = new System.Drawing.Point(422, 70);
            this.num6Lbl.Name = "num6Lbl";
            this.num6Lbl.Size = new System.Drawing.Size(13, 13);
            this.num6Lbl.TabIndex = 5;
            this.num6Lbl.Text = "6";
            this.num6Lbl.Click += new System.EventHandler(this.label6_Click);
            // 
            // num5Lbl
            // 
            this.num5Lbl.AutoSize = true;
            this.num5Lbl.Location = new System.Drawing.Point(366, 70);
            this.num5Lbl.Name = "num5Lbl";
            this.num5Lbl.Size = new System.Drawing.Size(13, 13);
            this.num5Lbl.TabIndex = 6;
            this.num5Lbl.Text = "5";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(474, 70);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 16);
            this.label8.TabIndex = 7;
            this.label8.Text = "Bonus";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // resultLbl
            // 
            this.resultLbl.AutoSize = true;
            this.resultLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.resultLbl.Location = new System.Drawing.Point(148, 159);
            this.resultLbl.Name = "resultLbl";
            this.resultLbl.Size = new System.Drawing.Size(53, 16);
            this.resultLbl.TabIndex = 8;
            this.resultLbl.Text = "Result";
            this.resultLbl.Click += new System.EventHandler(this.resultLbl_Click);
            // 
            // generateBtn
            // 
            this.generateBtn.Location = new System.Drawing.Point(263, 221);
            this.generateBtn.Name = "generateBtn";
            this.generateBtn.Size = new System.Drawing.Size(75, 23);
            this.generateBtn.TabIndex = 9;
            this.generateBtn.Text = "Generate";
            this.generateBtn.UseVisualStyleBackColor = true;
            this.generateBtn.Click += new System.EventHandler(this.generateBtn_Click);
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(353, 221);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(75, 23);
            this.saveBtn.TabIndex = 10;
            this.saveBtn.Text = "Save";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(151, 95);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(32, 20);
            this.textBox1.TabIndex = 11;
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(208, 95);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(32, 20);
            this.textBox2.TabIndex = 12;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(263, 95);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(32, 20);
            this.textBox3.TabIndex = 13;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(319, 95);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(32, 20);
            this.textBox4.TabIndex = 14;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(369, 95);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(32, 20);
            this.textBox5.TabIndex = 15;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(425, 95);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(32, 20);
            this.textBox6.TabIndex = 16;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(477, 95);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(32, 20);
            this.textBox7.TabIndex = 17;
            // 
            // statusLbl
            // 
            this.statusLbl.Location = new System.Drawing.Point(0, 287);
            this.statusLbl.Name = "statusLbl";
            this.statusLbl.Size = new System.Drawing.Size(675, 22);
            this.statusLbl.TabIndex = 18;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 309);
            this.Controls.Add(this.statusLbl);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.generateBtn);
            this.Controls.Add(this.resultLbl);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.num5Lbl);
            this.Controls.Add(this.num6Lbl);
            this.Controls.Add(this.num4Lbl);
            this.Controls.Add(this.num3Lbl);
            this.Controls.Add(this.num2Lbl);
            this.Controls.Add(this.num1Lbl);
            this.Controls.Add(this.titleLbl);
            this.Name = "Form1";
            this.Text = "Lottery Number Generator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label titleLbl;
        private System.Windows.Forms.Label num1Lbl;
        private System.Windows.Forms.Label num2Lbl;
        private System.Windows.Forms.Label num3Lbl;
        private System.Windows.Forms.Label num4Lbl;
        private System.Windows.Forms.Label num6Lbl;
        private System.Windows.Forms.Label num5Lbl;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label resultLbl;
        private System.Windows.Forms.Button generateBtn;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.StatusStrip statusLbl;
    }
}

