﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Configuration;
//using System.Threading.Tasks;

/**
 * @author: Lionel F. Mangoua
 * Date: 01/05/2015
 * Description: Small App to generate sorted Lottery winning 6 numbers + Bonus number, 
 * display them and save them in the database. 
 * */

namespace LotteryNumberGenerator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        Random m_random = new Random();
        int[] selectedNumbers = new int[7];

        // Generate button
        private void generateBtn_Click(object sender, EventArgs e)
        {
            // Assign a random number For each array member
            for (int numberCount = 0; numberCount < selectedNumbers.Length; numberCount++)
            {
                int newNumber = m_random.Next(1, 50);
                for (int i = 0; i < numberCount; i++)
                {
                    if (newNumber == selectedNumbers[i])
                    {
                        // MessageBox.Show(“A duplicate (” + newNumber + “) has occured”);
                        newNumber = m_random.Next(1, 50); // Assign a new number
                        // MessageBox.Show(“Replacing with ” + newNumber);
                        continue; // Test again
                    }
                }
                selectedNumbers[numberCount] = newNumber;
            }
            // The bonus number is placed in the 7th Textbox before sorting
            textBox7.Text = selectedNumbers[6].ToString();

            // Create and populate intemediary array which we can exclude the bonus number from
            int[] nonBonusNumbersArray = new int[6];

            for (int x = 0; x < nonBonusNumbersArray.Length; x++)
            {
                nonBonusNumbersArray[x] = selectedNumbers[x];
            }

            // Sort the intemediary array
            Array.Sort(nonBonusNumbersArray);

            // Sorted numbers are placed in non bonus Textboxes
            textBox1.Text = nonBonusNumbersArray[0].ToString();
            textBox2.Text = nonBonusNumbersArray[1].ToString();
            textBox3.Text = nonBonusNumbersArray[2].ToString();
            textBox4.Text = nonBonusNumbersArray[3].ToString();
            textBox5.Text = nonBonusNumbersArray[4].ToString();
            textBox6.Text = nonBonusNumbersArray[5].ToString();

            // Display result on the screen
            resultLbl.Text = "";
            resultLbl.Text += "Your new WINNING numbers are: \n\n";
            resultLbl.Text += nonBonusNumbersArray[0].ToString();
            resultLbl.Text += ", ";
            resultLbl.Text += nonBonusNumbersArray[1].ToString();
            resultLbl.Text += ", ";
            resultLbl.Text += nonBonusNumbersArray[2].ToString();
            resultLbl.Text += ", ";
            resultLbl.Text += nonBonusNumbersArray[3].ToString();
            resultLbl.Text += ", ";
            resultLbl.Text += nonBonusNumbersArray[4].ToString();
            resultLbl.Text += ", ";
            resultLbl.Text += nonBonusNumbersArray[5].ToString();
            resultLbl.Text += " (Bonus: ";
            resultLbl.Text += selectedNumbers[6].ToString();
            resultLbl.Text += ")";
        }

        // Save button
        private void saveBtn_Click(object sender, EventArgs e)
        {
            // Create the connection to the resource!
            //SqlConnection con = new SqlConnection(@"Data Source=.\SQLEXPRESS;AttachDbFilename=C:\Users\Lionel\Documents\lotteryDB.mdf;Integrated Security=True;Connect Timeout=30;User Instance=True");// connection string
            SqlConnection con = new SqlConnection("Data Source= localHost;Initial Catalog = lotteryDB;Trusted_Connection = True;Connect Timeout=30;");// connection string

            try
            {
                con.Open();// Open connection
                statusLbl.Text = "Connection Successful!";

                SqlCommand cmd = con.CreateCommand();// Create command
                cmd.CommandText = "Insert into LotteryNumbersTest ([num1],[num2],[num3],[num4],[num5],[num6],[bonus]) values ('" + textBox1.Text + "','" + textBox2.Text + "','" + textBox3.Text + "','" + textBox4.Text + "','" + textBox5.Text + "','" + textBox6.Text + "','" + textBox7.Text + "')";

                try
                {
                    cmd.ExecuteNonQuery();
                    statusLbl.Text = "Record Saved Successfully!";
                }
                catch (Exception)
                {
                    statusLbl.Text = "Query Execution Fail";
                }
                con.Close();// Close connection
            }
            catch (Exception)
            {
                statusLbl.Text = "Connection Fail";
            }

        }

        private void resultLbl_Click(object sender, EventArgs e)
        {

        }
    }
}
